# gnumakefile
#
# This application requires pandoc, latex, pdfunite, tex, openssl
# and gpg.
#
# todo:
# - i2p static site generator
# - tor static site generator
# - freenet static site generator
# - tarball deployment
# - twitter announces
# - telegram announces
# - deployment helper with tor/i2p

# newspaper name
TITLE = leglasen

# default build directory
OUTPUT ?= _output

# this should be updated when new volume is added
VOLUMES ?= 			\
	volume-00008 		\
	volume-00007		\
	volume-00006	 	\
	volume-00005	 	\
	volume-00004	 	\
	volume-00003	 	\
	volume-00002		\
	volume-00001

# variable helper
PDFS 		=
PDFS_SIGN 	=
PDFS_CKSUM 	=
HTMLS 		=
HTMLS_SIGN 	=
HTMLS_CKSUM 	=
EPUBS 		=
EPUBS_SIGN 	=
EPUBS_CKSUM 	=
CLEANERS 	=

# command helper
CHECKSUM ?= sha256
GPG_SIGN = gpg --detach-sign -a
OPENSSL_SIGN = openssl dgst -$(CHECKSUM)

# main macro used to generated targets
define VOLUME_template =
# initialize build environment
$$(OUTPUT)/$(1).tex: $$(OUTPUT)/assets $$(OUTPUT)/bibliographie.bib $$(OUTPUT)/newspaper-mod.sty
	cp volumes/$(1).tex $$@

# generate pdf targets
PDFS += $(1).pdf
$(1).pdf: $$(OUTPUT)/$(1).pdf
$$(OUTPUT)/$(1).pdf: $$(OUTPUT)/$(1).tex
	cd $$(OUTPUT) && latex $(1)
	cd $$(OUTPUT) && bibtex $(1)
	cd $$(OUTPUT) && latex $(1)
	cd $$(OUTPUT) && pdflatex $(1)

# generate html targets
HTMLS += $(1).html
$(1).html: $$(OUTPUT)/$(1).html
$$(OUTPUT)/$(1).html: $$(OUTPUT)/$(1).tex
	pandoc -s $$(OUTPUT)/$(1).tex -t html4 -o $$@

# generate epub targets
EPUBS += $(1).epub
$(1).epub: $$(OUTPUT)/$(1).epub
$$(OUTPUT)/$(1).epub: $$(OUTPUT)/$(1).tex
	pandoc -s $$(OUTPUT)/$(1).tex -t epub -o $$@

# checksum pdf files
PDFS_CKSUM += $(1).pdf.$$(CHECKSUM)
$(1).pdf.$$(CHECKSUM): $$(OUTPUT)/$(1).pdf.$$(CHECKSUM)
$$(OUTPUT)/$(1).pdf.$$(CHECKSUM): $$(OUTPUT)/$(1).pdf
	cd $$(OUTPUT) && $$(OPENSSL_SIGN) -out $(1).pdf.$$(CHECKSUM) $(1).pdf

# checksum html files
HTMLS_CKSUM += $(1).html.$$(CHECKSUM)
$(1).html.$$(CHECKSUM): $$(OUTPUT)/$(1).html.$$(CHECKSUM)
$$(OUTPUT)/$(1).html.$$(CHECKSUM): $$(OUTPUT)/$(1).html
	cd $$(OUTPUT) && $$(OPENSSL_SIGN) -out $(1).html.$$(CHECKSUM) $(1).html

# checksum epub files
EPUBS_CKSUM += $(1).epub.$$(CHECKSUM)
.PHONY += $(1).epub.$$(CHECKSUM)
$(1).epub.$$(CHECKSUM): $$(OUTPUT)/$(1).epub.$(CHECKSUM)
$$(OUTPUT)/$(1).epub.$$(CHECKSUM): $$(OUTPUT)/$(1).epub
	cd $$(OUTPUT) && $$(OPENSSL_SIGN) -out $(1).epub.$$(CHECKSUM) $(1).epub

ifndef NOSIGN
# sign pdf files
PDFS_SIGN += $(1).pdf.asc
$(1).pdf.asc: $$(OUTPUT)/$(1).pdf.asc
$$(OUTPUT)/$(1).pdf.asc: $$(OUTPUT)/$(1).pdf
	$$(GPG_SIGN) $$(OUTPUT)/$(1).pdf

# sign html files
HTMLS_SIGN += $(1).html.asc
$(1).html.asc: $$(OUTPUT)/$(1).html.asc
$$(OUTPUT)/$(1).html.asc: $$(OUTPUT)/$(1).html
	$$(GPG_SIGN) $$(OUTPUT)/$(1).html

# sign epub files
EPUBS_SIGN += $(1).epub.asc
.PHONY += $(1).epub.asc
$(1).epub.asc: $$(OUTPUT)/$(1).epub.asc
$$(OUTPUT)/$(1).epub.asc: $$(OUTPUT)/$(1).epub
	$$(GPG_SIGN) $$(OUTPUT)/$(1).epub

# generate signature cleaner
.PHONY += clean-signature-$(1)
clean-signature-$(1):
	rm $$(OUTPUT)/$(1)*.asc
endif

# generate targets helpers
.PHONY += $(1)
ifndef NOSIGN
$(1): $(1).pdf $(1).pdf.asc $(1).pdf.$$(CHECKSUM) 	\
	$(1).html $(1).html.asc $(1).html.$$(CHECKSUM)	\
	$(1).epub $(1).epub.asc $(1).epub.$$(CHECKSUM)
else
$(1): $(1).pdf $(1).pdf.$$(CHECKSUM) 			\
	$(1).html $(1).html.$$(CHECKSUM)		\
	$(1).epub $(1).epub.$$(CHECKSUM)
endif

# generate cleaner
.PHONY += clean-$(1)
CLEANERS += clean-$(1)
ifndef NOSIGN
clean-$(1): clean-pdf-$(1) clean-html-$(1) clean-epub-$(1) clean-checksum-$(1) clean-signature-$(1) clean-logs-$(1)
else
clean-$(1): clean-pdf-$(1) clean-html-$(1) clean-epub-$(1) clean-checksum-$(1) clean-logs-$(1)
endif

# generate pdf cleaner
.PHONY += clean-pdf-$(1)
clean-pdf-$(1):
	rm $$(OUTPUT)/$(1).pdf

# generate epub cleaner
.PHONY += clean-epub-$(1)
clean-epub-$(1):
	rm $$(OUTPUT)/$(1).epub

# generate html cleaner
.PHONY += clean-html-$(1)
clean-html-$(1):
	rm $$(OUTPUT)/$(1).html


# generate checksum cleaner
.PHONY += clean-checksum-$(1)
clean-checksum-$(1):
	rm $$(OUTPUT)/$(1)*.$$(CHECKSUM)

# generate remaining files cleaner
.PHONY += clean-logs-$(1)
clean-logs-$(1):
	rm $$(OUTPUT)/$(1)*
endef

# include extra-targets
include .mk/*.mk

# usage target
.PHONY += help
help:
	@echo "Usage: make [all|html|pdf|epub|volume-XXXXX|compilation|sign|checkum|tarball|clean|help|help-full]"

# full usage target
.PHONY += help-full
help-full: help
	@echo "  all: build the whole project"
	@echo "  html: build only html files"
	@echo "  pdf: build only pdf files"
	@echo "  epub: build only epub files"
	@echo "  volume-XXXXX: build one volume only, where 'X' is a number"
	@echo "  compilation: build a compilation"
	@echo "  sign: generate files PGP signature"
	@echo "  checksum: generate files checksum"
	@echo "  tarball: generate tar.gz and zip file"
	@echo "  clean: remove all files present in $(OUTPUT)"

# build default output directory
$(OUTPUT):
	mkdir $@

# copy assets (images/styles/binaries)
$(OUTPUT)/assets: $(OUTPUT)
	cp -r assets $(OUTPUT)

# copy bibliography
$(OUTPUT)/bibliographie.bib:
	cp extra/bibliographie.bib $@

# copy style (first volumes only)
$(OUTPUT)/newspaper-mod.sty:
	cp extra/newspaper-mod.sty $@

# generate targets with previously defined template
$(foreach volume,$(VOLUMES),$(eval $(call VOLUME_template,$(volume))))

# tarbal generator
$(OUTPUT)/volumes.tar.gz: all
	tar czvf $@ 			\
		$(OUTPUT)/*.pdf 	\
		$(OUTPUT)/*.epub 	\
		$(OUTPUT)/*.html	\
		$(OUTPUT)/*.rss		\
		$(OUTPUT)/*.asc		\
		$(OUTPUT)/*.$(CHECKSUM)

# zip generator
$(OUTPUT)/volumes.zip: all
	zip -r $@ 			\
		$(OUTPUT)/*.pdf 	\
		$(OUTPUT)/*.epub 	\
		$(OUTPUT)/*.html	\
		$(OUTPUT)/*.rss		\
		$(OUTPUT)/*.asc		\
		$(OUTPUT)/*.$(CHECKSUM)

# pdf compilation
.PHONY += compilation-pdf
compilation-pdf: $(OUTPUT)/$(TITLE).pdf compilation-sign-pdf compilation-checksum-pdf
$(OUTPUT)/$(TITLE).pdf: pdf
	pdfunite $(OUTPUT)/volume-*.pdf $@

# pdf compilation signature
.PHONY += compilation-sign-pdf
compilation-sign-pdf: $(OUTPUT)/$(TITLE).pdf.asc
$(OUTPUT)/$(TITLE).pdf.asc: $(OUTPUT)/$(TITLE).pdf
	$(GPG_SIGN) $(OUTPUT)/$(TITLE).pdf

# pdf compilation checksum
.PHONY += compilation-checksum-pdf
compilation-checksum-pdf: $(OUTPUT)/$(TITLE).pdf.$(CHECKSUM)
$(OUTPUT)/$(TITLE).pdf.$(CHECKSUM): $(OUTPUT)/$(TITLE).pdf
	$(OPENSSL_SIGN) -out $@ $(OUTPUT)/$(TITLE).pdf

# epub compilation
.PHONY += compilation-epub
compilation-epub: $(OUTPUT)/$(TITLE).epub
$(OUTPUT)/$(TITLE).epub: epub
	@echo "$@ not supported yet"

# html compilation
.PHONY += compilation-html
compilation-html: $(OUTPUT)/$(TITLE).html
$(OUTPUT)/$(TITLE).html: html
	@echo "$@ not supported yet"

# rss generator
# todo: create rss generator script
$(OUTPUT)/index.rss:
	cat assets/rss/header.rss > $@
	for volume in $(VOLUMES); do					\
		rss=volumes/$${volume}.rss;				\
		pdf_length=$$(ls -l $(OUTPUT)/$${volume}.pdf | awk '{ print $$5 '});	\
		epub_length=$$(ls -l $(OUTPUT)/$${volume}.epub| awk '{ print $$5 '});	\
		html_length=$$(ls -l $(OUTPUT)/$${volume}.html | awk '{ print $$5 '});	\
		if test -f "$${rss}"; then 				\
			cat "$${rss}" 					\
			| sed -e "s/%VOLUME%/$${volume}/g" 		\
			      -e "s/%PDF_LENGTH%/$${pdf_length}/g"	\
			      -e "s/%HTML_LENGTH%/$${html_length}/g"	\
			      -e "s/%EPUB_LENGTH%/$${epub_length}/g"	\
			>> $@; 						\
		else							\
			echo "rss file missing for $${volume}" ;	\
		fi;							\
		done
	cat assets/rss/footer.rss >> $@

# index generator
# todo: create index generator script
$(OUTPUT)/index.html: $(OUTPUT)/index.rss
	cat assets/html/header.html > $@
	echo "<center>" >> $@
	echo "<h1>Le Glasen</h1>" >> $@
	echo "<p>" >> $@
	echo "Sonnons le Glas de la Soumission" >> $@
	echo "</p>" >> $@
	echo "</center>" >> $@
	for volume in $(VOLUMES); do 							\
		echo "<h2>$${volume}</h2>" >> $@;					\
		cat volumes/$${volume}.telegram.md | pandoc -f markdown -t html >> $@;	\
		echo "<ul>" >> $@; 							\
		echo "<li>" >> $@; 							\
		echo "HTML: <a href='$${volume}.html'>$${volume}.html</a>" >> $@;	\
		echo "(<a href='$${volume}.html.asc'>signature</a>," >> $@;		\
		echo "<a href='$${volume}.html.$(CHECKSUM)'>checksum</a>)" >> $@;	\
		echo "</li>" >> $@;							\
		echo "<li>" >> $@; 							\
		echo "PDF: <a href='$${volume}.pdf'>$${volume}.pdf</a>" >> $@;		\
		echo "(<a href='$${volume}.pdf.asc'>signature</a>," >> $@;		\
		echo "<a href='$${volume}.pdf.$(CHECKSUM)'>checksum</a>)" >> $@;	\
		echo "</li>" >> $@;							\
		echo "<li>" >> $@; 							\
		echo "EPUB: <a href='$${volume}.epub'>$${volume}.epub</a>" >> $@;	\
		echo "(<a href='$${volume}.epub.asc'>signature</a>," >> $@;		\
		echo "<a href='$${volume}.epub.$(CHECKSUM)'>checksum</a>)" >> $@;	\
		echo "</li>" >> $@;							\
		echo "</ul>" >> $@; 							\
		done
	echo "<h2>Compilation</h2>" >> $@
	echo "<p>L'intégralité des volumes est disponible au format PDF.</p>" >> $@
	echo "<ul>" >> $@
	echo "<li>" >> $@
	echo "<a href='leglasen.pdf'>Tous les volumes</a>" >> $@
	echo "(<a href='leglasen.pdf.asc'>signature</a>," >> $@
	echo "<a href='leglasen.pdf.$(CHECKSUM)'>checksum</a>)" >> $@
	echo "</li>" >> $@
	echo "</ul>" >> $@
	echo "<h2>Contact</h2>" >> $@
	echo "<center>" >> $@
	echo "<p><a href='https://t.me/leglasen'>telegram</a>" >> $@
	echo "| <a href='https://leglasen.org/'>website</a>" >> $@
	echo "| <a href='https://twitter.com/Le_Glasen'>twitter</a>" >> $@
	echo "| <a href='http://leglasen.i2p'>i2p</a>" >> $@
	echo "| <a href='http://q6tqm2klaavlsfqjci2naul2nj57fbxvkwujmqdv53plhdxxi6mq.b32.i2p/'>i2p (b32)</a>" >> $@
	echo "| <a href='http://5jakfnvbfxcn32wtjqyahvwxno3dflueqenvbzn76z7dqpbn2lsemzyd.onion'>tor</a></p>" >> $@
	echo "<p><a href=''>leglasen [at] protonmail [dot] com</a></p>" >> $@
	echo "<pre>" >> $@
	cat pgp.asc >> $@
	echo "</pre>" >> $@
	echo "</center>" >> $@
	cat assets/html/footer.html >> $@

# full pdf targets helper
.PHONY += pdf
pdf: $(PDFS)

# full html targets helper
.PHONY += html
html: $(HTMLS)

# full epub targets helper
.PHONY += epub
epub: $(EPUBS)

ifndef NOSIGN
# sign pdf
.PHONY += sign-pdf
sign-pdf: $(PDFS_SIGN)

# sign html
.PHONY += sign-html
sign-html: $(HTMLS_SIGN)

# sign epub
.PHONY += sign-epub
sign-epub: $(EPUBS_SIGN)
endif

# checksum pdf
.PHONY += checksum-pdf
checksum-pdf: $(PDFS_CKSUM)

# checksum html
.PHONY += checksum-html
checksum-html: $(HTMLS_CKSUM)

# checksum epub
.PHONY += checksum-epub
checksum-epub: $(EPUBS_CKSUM)

# make a full book
.PHONY += compilation
compilation: compilation-pdf compilation-epub compilation-html

# sign all
.PHONY += sign
sign: sign-pdf sign-html sign-epub

# checksum all
.PHONY += checksum
checksum: checksum-pdf checksum-html checksum-epub

# index html
.PHONY += index
index: $(OUTPUT)/index.html

# main target helper
.PHONY += all
ifndef NOSIGN
all: pdf html epub compilation sign checksum index
else
all: pdf html epub compilation checksum index
endif

# tarball
.PHONY += tarball
tarball: $(OUTPUT)/volumes.tar.gz $(OUTPUT)/volumes.zip

# remove all assets
.PHONY += clean-assets
clean-assets:
	rm -rf $(OUTPUT)/assets
	
# remove remaining files
clean: $(CLEANERS) clean-assets
	rm $(OUTPUT)/*
