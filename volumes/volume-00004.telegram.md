---
telegram:
  published: https://t.me/leglasen/26
---

Le 18 septembre 2021 - Volume 4 - Crise Internationale

La situation actuelle n'est pas limité à la France ou aux pays
européens. Des actions similaires, parfois synchronisées, se déroulent
dans pratiquement toutes les régions du monde. La propagande, les
dérives autoritaires, le non respects des lois et l'écrasement de la
population est partagé par tous. Une guerre contre les peuples d'une
puissance inégalé est en train de se dérouler. À une telle
coordination entre les puissants, une solidarité entre les peuples
doit se créer. Il nous foudra un jour outre-passer les barrières de la
langue, outre-passer les différences culturelles ou sociales, et créer
une une action commune internationale.

post-scriptum: le journal a été publié avec une journée de
retard. Nous nous excusons pour ce délai. Bonne lecture et bon
partage.
