---
telegram:
  published: https://t.me/leglasen/38
---

Le 22 octobre 2021 - volume 8 - Société Fêlée

Depuis de nombreuses années des auteurs alertes sur le milieu de la
santé et de ses dérives. Cet article est dédié à ces personnages bien
trop peu connus qui luttent dans l'ombre. Bien souvent victime de
raillerie, avec ce que la population mondiale vie aujourd'hui, ils
n'étaient pas si loin de la vérité… Pour lutter contre la censure à
venir, Le Glasen se diffuse sur 2 nouveaux réseaux totalement
alternative, tor et i2p. Vous trouverez à la suite les liens qui vous
permettront de retrouver les articles au format PDF, EPUB et
HTML. D'autres services centrés sur l'anonymat et le partage devraient
voir prochainement le jour.

• Adresse tor: http://5jakfnvbfxcn32wtjqyahvwxno3dflueqenvbzn76z7dqpbn2lsemzyd.onion

• Adresse i2p: http://leglasen.i2p

• Adresse i2p (b32): http://q6tqm2klaavlsfqjci2naul2nj57fbxvkwujmqdv53plhdxxi6mq.b32.i2p

Bonne lecture ,
La rédaction du Glasen.
