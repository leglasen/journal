---
telegram:
  published: https://t.me/leglasen/32
---

Le 1er octobre 2021 - Volume 6 - Brèves sans Trêves

Des chiffres. Encore des chiffres.Toujours des chiffres. C'est ainsi
que va le monde, ces statistiques qui nous font oublier que nous
sommes bien plus que des numéros, mais des humains. Pourtant, sans ces
symboles numériques, pas de remise en cause possible, pas de
perspective. Depuis plus d'un mois, Le Glasen s'efforce de résumer
l'actualité, d'offrir aux lecteurs un condensé des faits. Cette
semaine a été plus chargé que les autres. De nombreux pays commencent
à retirer les restrictions, de plus en plus de questions sont
posées. La lutte n'est pas finie, mais l'histoire avance et avec elle
la vérité.

Post-Scriptum: de nombreux documents, ressources, références, ouvrages
et livres sont disponibles sur https://t.me/leglasen_doc. N'hésitez
pas à en profiter, à les partager et à les lire. Courage et merci de
votre fidélité.
