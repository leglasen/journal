---
telegram:
  published: https://t.me/leglasen/5
---

Le 27 août 2021 - Volume 1 - Naissance du Glasen
 
Le "Glasen" ou "coup de cloche" permettait d'indiquer l'heure aux
marins. Toutes les 30 minutes la cloche était sonnée. Au bout de 8
coups de cloche, un autre membre de l'équipage se relayait pour la
faire sonner.
 
Le navire France est en train de sombrer, le capitaine du navire, élu
"démocratiquement", n'a que faire de ses matelots. Nous, simples
marins, restons sur le navire partant à la dérive. Relayons nous pour
faire sonner le glas, informons nous et partageons ce que nous savons.
 
Voici donc la naissance du "Glasen", un micro-journal imprimé sur une
feuille A4 recto-verso, dont l'objectif est de diffuser l'actualité
sur les crises de notre époque. Plus largement, il a aussi pour
vocation de réinformer et rééduquer la population pour que cette
dernière puisse reprendre en main sa destinée. Né dans la sombre
période de la crise sanitaire du coronavirus, en l'an 2021, ce journal
sera disponible tous les vendredi soir, avant les manifestations...
