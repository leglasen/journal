---
telegram:
  published: https://t.me/leglasen/29
---

Le 24 septembre 2021 - Volume 5 - Résistance et Non-Violence

L'Australie saigne. L'Europe hurle. L'Amérique affronte. Les peuples
se lèvent. Avec eux, leur colère et leur espérance, leur désir de
changement et leur envie d'une vie nouvelle. Certains voudraient
retrouver le monde d'avant, définitivement perdu, que ce soit à l'aide
d'un pass sanitaire ou d'une vaccination obligatoire. Alors que
d'autres, manifestent et s'engagent dans une lutte contre une élite
corrompue. Divisé, morcelé, brisé… Pourtant, chaque citoyen regarde
dans la même direction, ils avancent sur le même chemin avec le même
désir brulant de retrouver une liberté sans chaines. Cette semaine, Le
Glasen est dédié à la lutte Non-Violente, et soutient de tout son cœur
la population australienne qui combat une dérive totalitaire sans
limites.

Soyez calme mais résolu. Préparez-vous à l'impossible. Entrainez-vous
à débattre et à combattre. Soyez autant de Thoreau, Ghandi ou
Mandela. Courage!
