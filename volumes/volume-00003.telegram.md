---
telegram:
  published: https://t.me/leglasen/13
---

Le 10 septembre 2021 - Volume 3 - Jeunesse en Danger

Depuis maintenant 18 mois, la population française vit dans un État de
non-droit. Subissant les affronts continus de la caste
politico-médiatique au moyen d'une une propagande répandue ad
nauseam. Le pays se découpe progressivement en un désert manichéen où
la pensée unique doit dominer sans aucune remise en question. Pendant
que le nombre de résistant augmente chaque week-end, que nos juristes
et avocats affrontent le maquis juridique et que les nombreux
mouvement citoyens s'organisent, il est peut-être judicieux de parler
des plus faibles. Les enfants et la jeunesse, dont leur condition
actuelle ne peut conduire qu'à plus de misère. Ces mois de disette
intellectuelle et physique n'ont fait qu'aggraver leur nombreux
soucis. Le Glasen est dédié cette semaine à cette frange silencieuse
et fragile, ces oubliés de la population et du gouvernement. Ceux qui
seront notre future. Que les dirigeants n'oublient jamais leurs
responsabilités dans cette affaire.
