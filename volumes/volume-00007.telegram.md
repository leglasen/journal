---
telegram:
  published: https://t.me/leglasen/36
---

Le 15 octobre 2021 - Doute et Confiance

Cela fait maintenant 4 mois qu'un mouvement social d'une ampleur et
d'une durée inégalée a vu le jour. Des manifestations spontanées se
sont mises en place en très peu de temps, regroupant toute la société
française, jeunes, retraités, travailleurs, ou étudiants. Après plus
de 15 week-end, un fait remarquable que beaucoup de médias oublient de
partager: il n'y a pratiquement eu aucune violence. Un pacifisme a
toute épreuve qui a aussi été permis grâce aux nombreuses structures
relayant les informations et partageant une vision non-violente de
l'engagement citoyen. Pour autant, pouvons-nous faire confiance
aveuglément en des inconnus? Devrions-nous refaire la même erreur que
nous avons commise avec les 40 dernières années de gouvernance opaque?
Peut-être est-il bon de prendre le temps d'étudier la situation et les
nouvelles icônes d'un mouvement qui semble avoir été créé pour durer.

Continuez de douter et bonne lecture,
La rédaction du Glasen
