# Le Glasen - Micro Journal Indépendant

 - Génération des volumes au format PDF
 - Génération des volumes au format EPUB
 - Génération des volumes au format HTML
 - Génération de la compilation des volumes au format PDF
 - Génération d'une archive au format tar.gz

## Prérequis

 - GNU Make
 - Latex 
 - Xetex
 - pdfunite
 - pandoc

## Compilation

Ce projet utilise [GNU Makefile](https://www.gnu.org/software/make/)
pour la création des volumes. Une aide suscinte pour l'utilisation se
fait au moment de la cible `help`

```
$ make help
Usage: make [all|html|pdf|epub|volume-XXXXX|compilation|sign|checkum|tarball|clean]
```

Ou afficher l'aide complète:

```
$ make help-full
Usage: make [all|html|pdf|epub|volume-XXXXX|compilation|sign|checkum|tarball|clean|help|help-full]
  all: build the whole project
  html: build only html files
  pdf: build only pdf files
  epub: build only epub files
  volume-XXXXX: build one volume only, where 'X' is a number
  compilation: build a compilation
  sign: generate files PGP signature
  checksum: generate files checksum
  tarball: generate tar.gz and zip file
  clean: remove all files present in _output
```

Pour compiler seulement un volume, deux méthodes possibles, soit
utilisant la cible `volume-XXXXX` où le nom du volume est stockée dans
la variable `VOLUMES` présente dans le fichier `Makefile`. Par
exemple, pour compiler seulement le `volume-00001`:

```sh
make volume-00001
```

Une autre méthode est de surcharger la variable `VOLUMES` de la façon
suivante. Il est possible alors de générer certains types de fichiers
en fonction du besoin. par exemple:

```sh
make VOLUMES=volume-00001 pdf epub html
```

Pour supprimer seulement certains fichiers lié à un volume (utile en
cas de développement ou de test):

```sh
make clean-volume-00001
```

Pour compiler l'intégralité des volumes au format pdf, html et epub,
la cible `all` peut-être utilisé.

```sh
make all
```

Pour nettoyer le dossier de destination:

```sh
make clean
```

## Contacts

 - e-mail: leglasen [at] protonmail [dot] com
 - website: https://leglasen.org
 - twitter: https://twitter.com/Le_Glasen
 - i2p: http://leglasen.i2p
 - tor: http://5jakfnvbfxcn32wtjqyahvwxno3dflueqenvbzn76z7dqpbn2lsemzyd.onion
